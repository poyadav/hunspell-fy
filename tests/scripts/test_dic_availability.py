#!/usr/bin/python3

import enchant

# Let's choose first alphabetical locale to test
lang = "fy_DE"
try:
    dic = enchant.request_dict(lang)
    print("Dictionary for {0} language is available for use".format(lang))
except enchant.errors.DictNotFoundError:
    print("Dictionary is not installed for use")
